# maskell

Functional Programming Project 2018-19

Resolving shortest path of a maze using Dijkstra algorithm (non-queued version).
The escape function takes a maze and print the same maze with a dotted path from
start to stop. If no (cross-like motion) solution are found, the function returns a friendly message.
The given maze is first validated before running finding a path.
A maze must respect the following conditions to be validated, otherwise an error will be raise:

- Must contains only: `X` `*` `@` ` ` (space)
- One start `*` and stop `@`
- Borders must be `X`
- All lines must have the same length

# Files

- `maskell.hs`: source code with all the Haskell functions
- `sample`: maze examples
- `bin`: compiled source to be executed

# How to 

Using **The Glorious Glasgow Haskell Compilation System, version 8.0.2**
## Execute

```bash
bin/maskell sample/maze.txt
```

## From source

```bash
ghc maskell.hs
./maskell sample/maze.txt
```

# Author

Hakim Boulahya (ULB)
