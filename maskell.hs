-- Author: Hakim Boulahya (ULB)
-- Description:
--
-- Resolving shortest path of a maze using Dijkstra algorithm (non-queued version).
-- The escape function takes a maze and print the same maze with a dotted path from
-- start to stop. If no (cross-like motion) solution are found, the function returns a friendly message.
-- The given maze is first validated before running finding a path.
-- A maze must respect the following conditions to be validated, otherwise an error will be raise:
--     - Must contains only: X * @ ' '
--     - One start and stop (*, @)
--     - Borders must be X
--     - All lines must have the same length
--
-- How to:
--
--  Using The Glorious Glasgow Haskell Compilation System, version 8.0.2
--
--  $ ghc maskell.hs
--  $ ./maskell maze.txt


import qualified System.Environment
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Maybe as Maybe
import qualified Data.List as List
import Debug.Trace

main :: IO ()
main = do [path] <- System.Environment.getArgs
          maze <- readFile path
          putStr $ unlines $ escape $ lines maze

escape :: [[Char]] -> [[Char]]
escape maze = solveWithDijstra maze

-- Maze aliases
type Cell = (Int, Int)
type Path = [Cell]
type Row = [Char]
type Maze = [Row]

-- Dijkstra algo aliases
type DistMap = [(Cell, Int)]
type PrevMap = [(Cell, Cell)]
type DijkstraOutput = (DistMap, PrevMap)

-- Constants
undefinedCell = (-1, -1) :: Cell
traceValue = '.'
borderValue = 'X'
sourceValue = '*'
targetValue = '@'
openValue = ' '

-- Shortest path using Dijkstra algorithm

solveWithDijstra :: Maze -> Maze
solveWithDijstra maze
    | undefinedCell `elem` path = ["Cannot reach the escape. You're doomed, friend."]
    | otherwise = tracePath maze path
    where (source, target) = validateMaze maze
          path = escapePath maze source target

tracePath :: Maze -> Path -> Maze
tracePath maze [] = maze
tracePath maze (cell@(i, j):remainingPath) = tracePath tracedMaze remainingPath
    where tracedMaze = take i maze ++ [newRow] ++ drop (i + 1) maze
          newRow = take j row ++ [traceValue] ++ drop (j + 1) row
          row = maze !! i

escapePath :: Maze -> Cell -> Cell-> Path
escapePath maze source target = List.delete target $ dijkstraPath source target prev
    where (dist, prev) = dijkstra maze cells initDist initPrev
          cells = buildCellKeys maze
          initDist = mapSet source 0 $ buildDist maze
          initPrev = mapSet source source $ buildPrev maze

dijkstraPath :: Cell -> Cell -> PrevMap -> Path
dijkstraPath source target prev
    | target == undefinedCell = [undefinedCell]
    | source == target = []
    | otherwise = dijkstraPath source prevCell prev ++ [target]
    where prevCell = mapGet target prev

dijkstra :: Maze -> [Cell] -> DistMap -> PrevMap -> DijkstraOutput
dijkstra maze [] dist prev = (dist, prev)
dijkstra maze vertices dist prev = dijkstra maze updatedVertices updatedDist updatedPrev
    where x@(u, d) = minDistVertex dist vertices
          (updatedDist, updatedPrev) = update maze dist prev x
          updatedVertices = List.delete u vertices

update :: Maze -> DistMap -> PrevMap -> (Cell, Int) -> DijkstraOutput
update maze dist prev x@(xcell, xdist) = updateNeighbors dist prev x $ validNeighbors maze xcell

updateNeighbors :: DistMap -> PrevMap -> (Cell, Int) -> [Cell] -> DijkstraOutput
updateNeighbors dist prev x@(xcell, xdist) [] = (dist, prev)
updateNeighbors dist prev x@(xcell, xdist) (neighbor:ns)
    | alt < ndist = updateNeighbors updatedDist updatedPrev x ns
    | otherwise = updateNeighbors dist prev x ns
    where alt = xdist + 1
          ndist = mapGet neighbor dist
          updatedDist = mapSet neighbor alt dist
          updatedPrev = mapSet neighbor xcell prev

-- Map functions

mapGet :: (Ord a) => a -> [(a, b)] -> b
mapGet k m = Maybe.fromJust $ Map.lookup k $ Map.fromList m

mapRemove :: (Ord a) => a -> [(a, b)] -> [(a, b)]
mapRemove k m = Map.toList $ Map.delete k $ Map.fromList m

mapAdd :: (a, b) -> [(a, b)] -> [(a, b)]
mapAdd e m = m ++ [e]

mapSet :: (Ord a) =>  a -> b -> [(a, b)] -> [(a, b)]
mapSet k v m = mapAdd (k, v) $ mapRemove k m

-- Maze functions

maxRows :: Maze -> Int
maxCols :: Maze -> Int
maxRows maze = length maze
maxCols maze = length $ maze !! 0

north :: Maze -> Cell -> Cell
south :: Maze -> Cell -> Cell
east :: Maze -> Cell -> Cell
west :: Maze -> Cell -> Cell
north maze cell@(i, j)
  | i == 0 = undefinedCell
  | otherwise = (i-1, j)
south maze cell@(i, j)
  | (==) (i+1) $ maxRows maze = undefinedCell
  | otherwise = (i+1, j)
east maze cell@(i, j)
  | (==) (j+1) $ maxCols maze = undefinedCell
  | otherwise = (i, j+1)
west maze cell@(i, j)
  | j == 0 = undefinedCell
  | otherwise = (i, j-1)

validCell :: Maze -> Cell -> Bool
validCell maze (i, j)
    | (i, j) == undefinedCell = False
    | value == openValue || value == targetValue || value == sourceValue = True
    | otherwise = False
    where value = maze !! i !! j

validCells :: Maze -> [Cell] -> [Cell]
validCells maze [] = error "Cannot validate cells if list is empty."
validCells maze (c:[]) = if isvalid then [c] else []
    where isvalid = validCell maze c
validCells maze (c:cs)
    | isvalid =  [c] ++ validCells maze cs
    | otherwise = validCells maze cs
    where isvalid = validCell maze c

neighbors :: Maze -> Cell -> [Cell]
neighbors maze cell = [north maze cell, south maze cell, east maze cell, west maze cell]

validNeighbors :: Maze -> Cell -> [Cell]
validNeighbors maze cell = validCells maze $ neighbors maze cell

cellsFor :: [Int] -> Int -> [Cell]
cellsFor (j:[]) i = [(i, j)]
cellsFor (j:js) i = [(i, j)] ++ (cellsFor js i)
concatSubList :: [[a]] -> [a]
concatSubList (sl:[]) = sl
concatSubList (sl:sls) = sl ++ (concatSubList sls)

-- Maze validation: stops if unvalid, returns (source, target) otherwise
validateMazeValues :: Maze -> Cell -> Cell -> [Cell] -> (Cell, Cell)
validateMazeValues maze source target [] = (source, target)
validateMazeValues maze source target (cell@(i, j):cells)
    | not isValidCellValue = error ("Cell value " ++ show cellValue ++ " at "
                                    ++ show cell ++  " is not valid. Must be either " ++ show validValues ++ ".")
    | source /= undefinedCell && cellValue == sourceValue = error ("Found to many stars " ++ show sourceValue ++  ".")
    | target /= undefinedCell && cellValue == targetValue = error ("Found to many escapes " ++ show targetValue ++  ".")
    | isBorder && cellValue /= borderValue = error ("Border at " ++ show cell ++ " must be " ++ show borderValue ++ ".")
    | otherwise = validateMazeValues maze foundSource foundTarget cells
    where cellValue = maze !! i !! j
          validValues = [borderValue, sourceValue, targetValue, openValue]
          isBorder = i == 0 || j == 0 || i == (maxRows maze) - 1 || j == (maxCols maze) - 1
          isValidCellValue = cellValue `elem` validValues
          foundSource = if cellValue == sourceValue then cell else source
          foundTarget = if cellValue == targetValue then cell else target


sameLinesLength :: Maze -> Bool
-- Three patterns to work with odd and even length matrix
sameLinesLength (x:y:[]) = length x == length y
sameLinesLength (x:y:z:[]) = length x == length y && length x == length z
sameLinesLength (x:y:xs) = length x == length y && sameLinesLength xs
-- Remaining patterns: empty and one element
sameLinesLength _ = True

validateMaze :: Maze -> (Cell, Cell)
validateMaze maze
    | not (sameLinesLength maze) = error "Maze lines do not have the same length."
    | source == undefinedCell = error ("No star " ++ show sourceValue ++ " found. Are you sure you are in the maze ?")
    | target == undefinedCell = error ("No escape " ++ show targetValue ++ " found. Goodbye..")
    | otherwise = (source, target)
    where (source, target) = validateMazeValues maze undefinedCell undefinedCell $ buildCellKeys maze

-- Dijkstra functions

buildCellKeys :: Maze -> [Cell]
buildCellKeys maze = concatSubList $ map (cellsFor [0..cols]) [0..rows]
    where rows = (maxRows maze) - 1
          cols = (maxCols maze) - 1

-- DistMap with inf. distances
buildDist :: Maze -> DistMap
-- Do not use maxBound :: Int, since the bound is the max and cycle back to the min. Int value
-- max dist = number of cells
buildDist maze = map (\cell -> (cell, (maxRows maze) * (maxCols maze))) $ buildCellKeys maze

-- PrevMap with undefined previous
buildPrev :: Maze -> PrevMap
buildPrev maze = map (\cell -> (cell, undefinedCell)) $ buildCellKeys maze

minDistVertex :: DistMap -> [Cell] -> (Cell, Int)
minDistVertex dist [] = error "Cannot find dist with available cells empty."
minDistVertex dist (cell:[]) = (cell, mapGet cell dist)
minDistVertex dist cells@(xcell:cs)
    | xdist < ydist = x
    | otherwise  = y
    where y@(ycell, ydist) = minDistVertex dist cs
          x = (xcell, xdist)
          xdist = mapGet xcell dist
